from lightfm import LightFM
from lightfm.data import Dataset
from nltk.corpus import stopwords
import datetime as dt
import lightfm.evaluation as ev
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

def date_str_to_dt(s):
    return dt.datetime.strptime(s, "%Y-%m-%d")

stop=stopwords.words('english') + ["via", "using", "based", " ", "towards"]

# I had to modify this repo but don't remember what i did. Probably include first names of authors.
os.chdir("/Users/josef/github.com/mahda/arxivscraper")
import arxivscraper

# Download articles from arxiv
scraper = arxivscraper.Scraper(date_from='2008-05-28',date_until='2018-08-14', category="stat")

t = scraper.scrape()

bad_tokens = [",", ":", " ", "-", "!", "\"", "(", ")"]

# Split articles in training and validation set

train_val_split = date_str_to_dt('2018-03-01')
train_set = []
val_set = []
for a in t:
    if (date_str_to_dt(a['created']) - train_val_split).days < 0:
        train_set.append(a)
    else:
        val_set.append(a)


# Parse arxiv data and create a list of words in the article title, and authors
def parser(articles):
    unique_tags = set()
    pairs = []
    for paper in articles:
        title_words = [a for a in paper['title'].split(" ") if a not in stop]
        authors = paper['authors']
        tags = paper['categories']
        [unique_tags.add(a) for a in tags.split(" ")]
        doi = paper['doi']
        url = paper['url']
        released = paper['created']
        for n in title_words:
            n_cleaned = "".join([c for c in n if c not in bad_tokens])
            for m in authors:
                if len(n_cleaned) > 0:
                    pairs.append([n_cleaned, m, tags, doi, url, released])
    columns = ["token", "author", "tags", "doi", "url", "released"]
    pairs_df = pd.DataFrame(pairs, columns=columns)
    pairs_df['released'] = pd.to_datetime(pairs_df['released'])
    return pairs_df


pairs_df = parser(train_set)
pairs_val = parser(val_set)

# Plots

pairs_tot = pd.concat([pairs_df, pairs_val], axis=0)

pairs_tot['month'] = pairs_tot.released.astype('datetime64[M]')

sns.set_style("darkgrid")
palt = sns.color_palette("husl", 8)

fig, ax = plt.subplots()
n=0
for a in ["deep", "clustering", "adversarial"]:
    d1 = pairs_tot[pairs_tot.token == a].groupby("month").url.nunique().reset_index()
    d1.plot(ax=ax, x="month", y="url", c=palt[n], label=a)
    n = n + 1
plt.xlim(('2012-01-01', '2018-05-01'))
plt.xlabel("Month")
plt.ylabel("Uses")
plt.savefig("token_uses.pdf")
plt.show()

# 

#
# Build dataset for model training
# 

counts = pairs_df.groupby(["token", "author"]).size().reset_index()

counts_val = pairs_val.groupby(["token", "author"]).size().reset_index()

# Item and user feature matrix

tags_list = pairs_df.tags.str.split(" ")

for a in unique_tags:
    pairs_df[a] = pairs_df.tags.str.find(a) > 0

item_tags = pairs_df.groupby(["token"]).apply(lambda x: x[list(unique_tags)].mean())

user_tags = pairs_df.groupby(["author"]).apply(lambda x: x[list(unique_tags)].mean())

item_feature_list = []
user_feature_list = []
for a in list(unique_tags)[:2]:
    i_list = [(i, {a: x[a]}) for i, x in item_tags.iterrows()]
    u_list = [(i, {a: x[a]}) for i, x in user_tags.iterrows()]
    item_feature_list = item_feature_list + i_list
    user_feature_list = user_feature_list + u_list

user_item_interaction = [(x['author'], {x['token']: 1}) for i, x in counts.iterrows()]
    
# Build LightFM embeddings

d = Dataset()

d.fit(counts['author'], counts['token'], np.unique(counts.token).tolist() + list(unique_tags), list(unique_tags))

(interactions, weights) = d.build_interactions(((x['author'], x['token'], x[0]) for i, x in counts.iterrows()))

item_matrix = d.build_item_features(item_feature_list)
user_matrix = d.build_user_features(user_feature_list + user_item_interaction)

# train model

model = LightFM(loss='warp', no_components=10),# user_alpha=0.00001, item_alpha=0.00001)
model.fit(interactions, user_matrix, item_matrix, epochs=30, num_threads=2, verbose=True)

# Validate

common_authors = set(pairs_val.author).intersection(pairs_df.author)
common_items = set(pairs_val.token).intersection(pairs_df.token)

counts_val = counts_val[counts_val.author.isin(common_authors) & counts_val.token.isin(common_items)]

(interactions_test, weights_test) = d.build_interactions(((x['author'], x['token'], x[0]) for i, x in counts_val.iterrows()))

user_item_interaction_test = [(x['author'], {x['token']: 1}) for i, x in counts_val.iterrows()]

item_matrix_test = d.build_item_features([uf for uf in item_feature_list if uf[0] in common_items])
user_matrix_test = d.build_user_features(user_item_interaction_test + [uf for uf in user_feature_list if uf[0] in common_authors])

# Training set
ev.precision_at_k(model, interactions, user_features=user_matrix, item_features=item_matrix).mean()

ev.recall_at_k(model, interactions, user_features=user_matrix, item_features=item_matrix).mean()

ev.auc_score(model, interactions, user_features=user_matrix, item_features=item_matrix).mean()

# Test set

ev.precision_at_k(model, interactions_test, user_features=user_matrix_test, item_features=item_matrix_test).mean()

ev.recall_at_k(model, interactions_test, user_features=user_matrix_test, item_features=item_matrix_test).mean()

ev.auc_score(model, interactions_test, user_features=user_matrix_test, item_features=item_matrix_test).mean()

# Validate tokens not seen

counts_val_not_seen = counts_val.merge(counts, on=["token","author"], how="left")
counts_val_not_seen = counts_val_not_seen[pd.isna(counts_val_not_seen["0_y"])]

(interactions_test2, weights_test2) = d.build_interactions(((x['author'], x['token'], 1) for i, x in counts_val_not_seen.iterrows()))

user_item_interaction_test2 = [(x['author'], {x['token']: 1}) for i, x in counts_val_not_seen.iterrows()]

item_matrix_test2 = d.build_item_features([uf for uf in item_feature_list if uf[0] in set(counts_val_not_seen.token)])
user_matrix_test2 = d.build_user_features(user_item_interaction_test2 + [uf for uf in user_feature_list if uf[0] in set(counts_val_not_seen.author)])

ev.precision_at_k(model, interactions_test2, train_interactions=interactions, user_features=user_matrix_test2, item_features=item_matrix_test2, k=10).mean()

ev.recall_at_k(model, interactions_test2, train_interactions=interactions, user_features=user_matrix_test2, item_features=item_matrix_test2, k=10).mean()

ev.auc_score(model, interactions_test2, train_interactions=interactions, user_features=user_matrix_test2, item_features=item_matrix_test2).mean()

roc_curve=[]
for k in range(10):
    print(k)
    prec = ev.precision_at_k(model, interactions_test2, train_interactions=interactions, user_features=user_matrix_test2, item_features=item_matrix_test2, k=k+3).mean()
    rec = ev.recall_at_k(model, interactions_test2, train_interactions=interactions, user_features=user_matrix_test2, item_features=item_matrix_test2, k=k+3).mean()
    roc_curve.append([prec, rec])

pd.DataFrame(roc_curve, columns=["x", "y"]).plot(x="x", y="y")
plt.xlabel("Precision")
plt.ylabel("Recall")
plt.savefig("precision_recall.pdf")
plt.close()

aucs=ev.auc_score(model, interactions_test2, train_interactions=interactions, user_features=user_matrix_test2, item_features=item_matrix_test2)

sns.distplot(aucs)
plt.xlabel("AUC")
plt.ylabel("Count")
plt.savefig("auc.pdf")
plt.close()

# Some xtra fun stuff

token_mappings = dict([[k, v] for v, k in d.mapping()[3].items()])

test_author = 'geoffrey e. hinton'
test_author = 'yann lecun'

test_author = 'jerome h. friedman'

words_used = np.unique(pairs_df[pairs_df.author==test_author].token)

(interactions_test, weights_test) = d.build_interactions(((test_author, x, 1) for x in np.unique(pairs_df.token) if x not in words_used))

item_matrix_test = d.build_item_features(item_feature_list)
user_matrix_test = d.build_user_features([uf for uf in user_feature_list if uf[0] == test_author] + [ui for ui in user_item_interaction if ui[0] == test_author])

ranks = model.predict_rank(interactions_test, user_features=user_matrix_test, item_features=item_matrix_test)
ranks.data *= ranks.data < 20
[token_mappings[a] for a in ranks.nonzero()[1]]

# Author similarity

np.dot(model.user_embeddings[d.mapping()[0]['jerome h. friedman']], model.user_embeddings[d.mapping()[0]['yann lecun']])

np.dot(model.user_embeddings[d.mapping()[0]['geoffrey e. hinton']], model.user_embeddings[d.mapping()[0]['yann lecun']])

# Iso map

import sklearn.manifold as mfl
import sklearn.cluster as cl

iso = mfl.Isomap(n_neighbors=10, n_components=2, n_jobs=4)
kmeans = cl.KMeans(n_clusters=4)

subset = pd.DataFrame(model.item_embeddings).sample(10000)

kmeans.fit(subset)

iso.fit(subset)

tst = pd.DataFrame(iso.transform(subset), columns=["x", "y"])

tst['cluster'] = kmeans.predict(subset)

tst.plot.scatter(x = "x", y="y", c="cluster", colormap="rainbow", alpha=0.1)
plt.show()

# T-sne
tsne = mfl.TSNE(n_components=2, verbose=2, perplexity=5)

tst = tsne.fit_transform(subset)

tst = pd.DataFrame(tst, columns=["x", "y"])

tst['cluster'] = kmeans.predict(subset)

tst.plot.scatter(x = "x", y="y", c="cluster", colormap="rainbow", alpha=0.1)
plt.show()
